import json
import requests


class Weather:

    def __init__(self):
        self.key = input("wpisz klucz do api")
        self.dates = {}
        self.user_date = ""
        self.raining_codes = ('t04n', 't04d', 't04n', 't04d', 't04n', 't04d', 't03n', 't03d', 't02n', 't02d', 't01n', 't01d')


    def menu(self):
        print()
        print("Please select a day to check a weather. From today to 16 days ahead")
        print("Use the format as in the example below")
        print("rrrr-mm-dd")
        self.user_date = input(': ')
        self.get()

        if self.user_date in self.dates:
            if self.dates[self.user_date] in self.raining_codes:
                print('Będzie padac')
            else:
                print("Nie bedzie padac")
        else:
            print('Nie wiadomo')


    def get(self):
        url = "https://weatherbit-v1-mashape.p.rapidapi.com/forecast/daily"
        querystring = {"lat":"38.5","lon":"-78.5"}
        headers = {
            "X-RapidAPI-Host": "weatherbit-v1-mashape.p.rapidapi.com",
            "X-RapidAPI-Key": f"{self.key}"
        }
        response = requests.get( url, headers=headers, params=querystring)

        for x in response.json()['data']:
            self.dates[x['valid_date']] = x['weather']['icon']
        


weather1 = Weather()
weather1.menu()